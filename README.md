# My project's README
To install dependencies just type
```bash
npm i
```
After this, you can start server. If you want start in dev mode (with nodemon) type
```bash
npm run dev
```
If you want to run in normal mode type
```bash
npm start
```