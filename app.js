const express = require('express');
const rss = require('rss-parser');
const request = require('request');
const cheerio = require('cheerio');
const nodeport = 3000;
const app = express();
const parseLink = "https://www.057.ua/rss";
app.set('view engine', 'pug');
app.locals.pretty = true;

// array of urls to array of html pages
function getHtml(urlsAr) {
    let reqPrAr = [];
    urlsAr.forEach((url) => {
        reqPrAr.push(new Promise((resolve, reject) => {
            request(url, (err, htmlRes, html) => {
                if (!err && htmlRes.statusCode == 200) {
                    resolve({ "page": html, "url": url });
                } else {
                    reject(err);
                }
            });
        }))
    });
    return Promise.all(reqPrAr);
}

// parse  rss link and return array of links on original entries
function readRSS(count, rssurl) {
    return new Promise((rssResolve, rssReject) => {
        rss.parseURL(rssurl, (err, parsed) => {
            if (err) {
                console.log('prasing error');
                console.log(err);
                rssReject(err);
                // res.send({ "message": err });
            } else {
                let entries = parsed.feed.entries.slice(0, count);
                let linksToParse = [];
                // console.log(parsed.feed.title);
                entries.forEach((entry) => {
                    //console.log(entry.title + ':' + entry.link);
                    linksToParse.push(entry.link);
                });
                rssResolve(linksToParse);
            }
        });
    })

}

// from html pages get titles, full entries, and images (if exist), convert it to array of objects
function parseHtmlEntries(htmls) {
    let entries = [];
    htmls.forEach((entry) => {
        const $ = cheerio.load(entry.page);
        const title = $('body > main > div.container-fluid > div.row.sect-mb > div > div > h1').text();
        let imgLink = '';
        if ($('body > main > div > div.row.sect-mb > div > div.article-photo.main').attr('style')) {
            imgLink = $('body > main > div > div.row.sect-mb > div > div.article-photo.main').attr('style').match(/'(.*?)'/)[1];
        }


        // const firstPostBlock = $('#content > div.conteiner.open_news > div.static > p:nth-child(1) > i').text();
        const otherPostBlocks = $('body > main > div.container-fluid > div:nth-child(3) > div.col-xs-12.col-md-8.col-lg-9 > div:nth-child(1) > div.col-xs-12.col-sm-9 > div > p');
        let post = '';
        otherPostBlocks.each((index, element) => {
            post += $(element).text();
        })
        const origLink = entry.url;
        entries.push({
            "title": title,
            "imgLink": imgLink,
            "post": post,
            "origLink": origLink
        });
    })
    return entries;
}
app.get('*/news/:count', (req, res) => {
    // number of requested entries
    const count = req.params.count;
    console.log('Requested count = ', count);
    readRSS(count, parseLink)
        .then(linksToParse => {
            return getHtml(linksToParse)
        })
        .then(htmls => {
            let entries = parseHtmlEntries(htmls);
            res.render('rss', { "entries": entries });
        })
        .catch(err => {
            console.log('some error', err);
            res.send(err);
        });
});
app.listen(nodeport, function() {
    console.log(parseLink + ' - parser listening on port ' + nodeport + '!');
});